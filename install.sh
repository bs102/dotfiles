#!/bin/bash
SCRIPT_DIR=$(cd $(dirname $0);pwd)
INSTALLED_VIM=0
INSTALLED_NVIM=0
NVIM_INIT="nvim/init.vim"

setup_vim(){
  rm -f "${SCRIPT_DIR}/vimrc" ~/.vimrc ~/.vim
  ln -si "${SCRIPT_DIR}/${NVIM_INIT}" "${SCRIPT_DIR}/vimrc"
  ln -si "${SCRIPT_DIR}/vimrc" ~/.vimrc
  ln -si "${SCRIPT_DIR}/nvim" ~/.vim
}

setup_nvim(){
  rm -f ~/.config/nvim
  ln -s "${SCRIPT_DIR}/nvim" ~/.config
}

if [ `which vim` ]; then INSTALLED_VIM=1; fi
if [ `which nvim` ]; then INSTALLED_NVIM=1; fi

if [ $INSTALLED_VIM = 0 ] && [ $INSTALLED_NVIM = 0 ]; then
  echo "error: please install vim or nvim"
  exit
fi

if [ $INSTALLED_VIM -eq 1 ]; then setup_vim; fi
if [ $INSTALLED_NVIM -eq 1 ]; then setup_nvim; fi

if [ `which tmux` ]; then
  rm -f ~/.tmux.conf
  ln -si "${SCRIPT_DIR}/tmux/tmux.conf" ~/.tmux.conf
fi

if [ `which mpv` ]; then
  rm -rf ~/.config/mpv
  ln -si "${SCRIPT_DIR}/mpv" ~/.config/mpv
fi
