@echo off
set SCRIPT_DIR=%~dp0

mklink %SCRIPT_DIR%\vimrc %SCRIPT_DIR%\nvim\init.vim
mklink %SCRIPT_DIR%\gvimrc %SCRIPT_DIR%\nvim\ginit.vim
mklink %LocalAppData%\nvim\init.vim %SCRIPT_DIR%\vimrc
mklink %LocalAppData%\nvim\ginit.vim %SCRIPT_DIR%\gvimrc
mklink %USERPROFILE%\.vimrc %SCRIPT_DIR%\vimrc
