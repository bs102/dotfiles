" functions.vim
function! IsWindows() abort
  return has('win32') || has('win64')
endfunction

function! IsCygwin() abort
  return has('win32unix')
endfunction

function! IsMainWindow() abort
  let l:current_win = winnr()
  let l:winarea = []
  for i in range(1, winnr('$'))
    call add(l:winarea, winwidth(i) * winheight(i))
  endfor
  if index(l:winarea, max(l:winarea))+1 == l:current_win
    return 1
  endif
  return 0
endfunction

function! ActionInMainWindow(cmd) abort
  if IsMainWindow()
    call execute(a:cmd)
  endif
endfunction
