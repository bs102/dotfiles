" plugins.vim
let s:running_editor = has('nvim') ? 'nvim' : 'vim'
let s:vim_pack_dir = expand('~/.cache/' . s:running_editor)
exe 'set packpath=' . s:vim_pack_dir

let s:vim_packager_cache_dir = s:vim_pack_dir . '/pack/packager'
let s:vim_packager_dir = s:vim_packager_cache_dir . '/opt/vim-packager'

if has('vim_starting')
  if !isdirectory(s:vim_packager_dir)
    execute '!git clone https://github.com/kristijanhusak/vim-packager' s:vim_packager_dir
  endif
endif

function! PackagerInit() abort
  packadd vim-packager

  call packager#init()
  call packager#add('kristijanhusak/vim-packager', {'type': 'opt'})

  " file (syntax, filetype, etc.)
  call packager#add('Shougo/context_filetype.vim')
  call packager#add('osyo-manga/vim-precious')
  call packager#add('cespare/vim-toml')
  call packager#add('fidian/hexmode')
  call packager#add('hail2u/vim-css3-syntax')
  call packager#add('othree/html5.vim')
  call packager#add('harenome/vim-mipssyntax')
  call packager#add('lervag/vimtex')
  call packager#add('Shougo/neco-vim')
  call packager#add('fatih/vim-go')
  call packager#add('ekalinin/Dockerfile.vim')
  call packager#add('udalov/kotlin-vim')
  call packager#add('tfnico/vim-gradle')
  call packager#add('pangloss/vim-javascript')
  call packager#add('mxw/vim-jsx')
  call packager#add('kovisoft/slimv')
  call packager#add('google/vim-jsonnet')

  " coding
  call packager#add('dense-analysis/ale')
  call packager#add('autozimu/LanguageClient-neovim', {'branch': 'next', 'do': 'bash install.sh'})
  call packager#add('cohama/lexima.vim')
  call packager#add('Shougo/neoinclude.vim')
  call packager#add('SirVer/ultisnips')
  call packager#add('ncm2/ncm2')
  call packager#add('ncm2/ncm2-path')
  call packager#add('ncm2/ncm2-vim')
  call packager#add('ncm2/ncm2-ultisnips')
  call packager#add('ncm2/ncm2-neoinclude')
  call packager#add('ncm2/ncm2-jedi')
  call packager#add('ncm2/ncm2-tern', {'do': 'npm install'})
  call packager#add('ncm2/ncm2-pyclang')
  call packager#add('heavenshell/vim-jsdoc')
  call packager#add('honza/vim-snippets')

  " tools
  call packager#add('thinca/vim-quickrun')
  call packager#add('Shougo/denite.nvim')
  call packager#add('lambdalisue/gina.vim')
  call packager#add('kassio/neoterm')
  call packager#add('easymotion/vim-easymotion')
  call packager#add('mbbill/undotree')
  call packager#add('Shougo/defx.nvim', {'do': ':UpdateRemotePlugins'})
  call packager#add('kana/vim-tabpagecd')
  call packager#add('kristijanhusak/defx-icons')

  " appearance
  call packager#add('rakr/vim-one')
  call packager#add('itchyny/lightline.vim')
  call packager#add('maximbaz/lightline-ale')

  " misc
  call packager#add('Shougo/neomru.vim')
  call packager#add('roxma/nvim-yarp')
  if !has("nvim") && has("timers") && has("python3")
    call packager#add('roxma/vim-hug-neovim-rpc')
  endif
  call packager#add('xolox/vim-misc')

endfunction

command! PackagerInstall call PackagerInit() | call packager#install()
command! PackagerUpdate call PackagerInit() | call packager#update()
command! PackagerClean call PackagerInit() | call packager#clean()
command! PackagerStatus call PackagerInit() | call packager#status()

" -- plugin config --
" fldian/hexmode
let g:hexmode_patterns = '*.bin,*.exe,*.o,*.jpg,*.gif,*.png'

" fatih/vim-go
noremap [VimGo] <Nop>
nmap <Leader>g [VimGo]
" run
nnoremap [VimGo]r :<C-u>GoRun<CR>
" build
nnoremap [VimGo]b :<C-u>GoBuild<CR>
" test
nnoremap [VimGo]t :<C-u>GoTest<CR>
" doc
nnoremap [VimGo]m :<C-u>GoDoc<CR>

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1

" dense-analysis/ale
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 0
let g:ale_lint_on_save = 1
let g:ale_open_list = 1
let g:ale_set_quickfix = 1

augroup vimrc-local
  autocmd!
  autocmd BufNewFIle,BufReadPost * call s:vimrc_local(expand('<afile>:p:h'))
augroup END
function! s:vimrc_local(loc)
  let files = findfile('.vimrc.local', escape(a:loc, ' ') . ';', -1)
  for i in reverse(filter(files, 'filereadable(v:val)'))
    source `=i`
  endfor
endfunction

let g:ale_linters = {'tex': ['mtxcheck'], 'cpp': ['ccls', 'clangtidy']}

" c
let g:ale_c_build_dir_names = ['build']
let g:ale_c_parse_compile_commands = 1
let g:ale_c_parse_makefile = 1

" cpp
let g:ale_cpp_gcc_options = '-std=c++17'
let g:ale_cpp_clang_options = '-std=c++17'
let g:ale_cpp_clangd_options = '-std=c++17'
let g:ale_cpp_clangtidy_options = '-std=c++17'
let g:ale_cpp_ccls_init_options = {
\   'cache': {
\       'directory': '/tmp/ccls/cache'
\   },
\   'clang': {
\       'extraArgs': [
\         "-isystem/Library/Developer/CommandLineTools/usr/include/c++/v1",
\         "-isystem/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include",
\         "-isystem/usr/local/include",
\         "-isystem/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.0/include",
\         "-isystem/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include",
\         "-isystem/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include",
\         "-isystem/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/System/Library/Frameworks"
\       ]
\   },
\   "compilationDatabaseDirectory": "build"
\ }

" autozimu/LanguageClient-neovim
let g:LanguageClient_diagnosticsSignsMax = 0
let g:LanguageClient_completionPreferTextEdit = 1
let g:LanguageClient_rootMarkers = {
  \ 'haskell': ['*.cabal'],
  \ 'kotlin': ['build.gradle', 'build.gradle.kts']
  \ }
if has('nvim')
  let g:LanguageClient_useVirtualText = 0
endif
let g:LanguageClient_hasSnippetSupport = 1

function! s:set_servercmds(lang, cmd)
  for m in a:lang
    if a:cmd[0] =~ '^tcp://' || executable(a:cmd[0])
      let g:LanguageClient_serverCommands[m] = a:cmd
      let g:available_lsp[m] = 1
    else
      let g:available_lsp[m] = 0
    endif
  endfor
endfunction

function! s:clangd_args_factory()
  let l:args = []
  if !executable('clangd')
    return l:args
  else
    call add(l:args, 'clangd')
  endif
  " check clangd option
  let l:clangd_help = system('clangd -help')
  if l:clangd_help =~ "enable-snippets" " clang 6.0
    call add(l:args, '-enable-snippets')
  elseif l:clangd_help =~ "header-insertion-decorators" " clang 7.0 or later
    call add(l:args, '-header-insertion-decorators=0')
  endif
  return l:args
endfunction

function! s:eclipse_jdt_ls_args_factory()
  let l:args = [
    \'java',
    \'-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044',
    \'-Declipse.application=org.eclipse.jdt.ls.core.id1',
    \'-Dosgi.bundles.defaultStartLevel=4',
    \'-Declipse.product=org.eclipse.jdt.ls.core.product',
    \'-noverify',
    \'-Xmx1G',
    \'-jar',
    \expand(g:lsp_eclipse_jdt_ls_dir . '/plugins/org.eclipse.equinox.launcher_*.jar'),
    \'-configuration',
    \g:lsp_eclipse_jdt_ls_dir . '/config_' . (has('win32') ? 'win' : has('macunix') ? 'mac' : 'linux'),
    \'-data',
    \expand('~/workspace'),
    \]
  let l:java_version = system("java -version 2>&1 | head -n 1 | awk -F '\"' '{print $2}'")
  if l:java_version >= 9
    call add(l:args, '--add-modules=ALL-system --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED')
  endif
  return l:args
endfunction

let g:available_lsp = {}
let g:LanguageClient_serverCommands = {}
"call s:set_servercmds(['c', 'cpp'], s:clangd_args_factory())
call s:set_servercmds(['rust'], ['rustup', 'run', 'stable', 'rls'])
call s:set_servercmds(['go'], ['go-langserver', '-gocodecompletion'])
if executable('java') && !empty(g:lsp_eclipse_jdt_ls_dir) && isdirectory(g:lsp_eclipse_jdt_ls_dir)
  call s:set_servercmds(['java'], s:eclipse_jdt_ls_args_factory())
endif
if !empty(g:lsp_kotlin_language_server_dir) && isdirectory(g:lsp_kotlin_language_server_dir)
  call s:set_servercmds(['kotlin'], [g:lsp_kotlin_language_server_dir . '/bin/kotlin-language-server'])
endif
call s:set_servercmds(['lisp'], ['cl-lsp'])

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Sirver/ultisnips
" https://github.com/ncm2/ncm2-ultisnips/issues/6#issuecomment-410186456
let g:UltiSnipsExpandTrigger = "<Plug>(ultisnips_expand_or_jump)"
let g:UltiSnipsJumpForwardTrigger = "<Plug>(ultisnips_expand_or_jump)"
let g:UltiSnipsJumpBackwardTrigger = "<S-Tab>"
let g:UltiSnipsRemoveSelectModeMappings = 0

" ncm2/ncm2
augroup ncm2
  autocmd!
  autocmd BufEnter * call ncm2#enable_for_buffer()
augroup END
set completeopt=noinsert,menuone,noselect
set shortmess+=c
inoremap <expr> <C-f> pumvisible() ? "\<C-n>" : "\<C-f>"
inoremap <expr> <C-d> pumvisible() ? "\<C-p>" : "\<C-d>"

" ncm2/ncm2-ultisnips
function! UltiSnipsExpandOrJumpOrTab()
  call UltiSnips#ExpandSnippetOrJump()
  if g:ulti_expand_or_jump_res > 0
    return ""
  else
    return "\<Tab>"
  endif
endfunction
inoremap <silent> <expr> <Tab> ncm2_ultisnips#expand_or("\<Plug>(ultisnips_try_expand)")
inoremap <silent> <Plug>(ultisnips_try_expand) <C-R>=UltiSnipsExpandOrJumpOrTab()<CR>
snoremap <silent> <Tab> <Esc>:call UltiSnips#ExpandSnippetOrJump()<cr>

" heavenshell/vim-jsdoc
nmap <silent> <C-l> <Plug>(jsdoc)
let g:jsdoc_allow_input_prompt = 1
let g:jsdoc_input_description = 1
let g:jsdoc_enable_es6 = 1

" thinca/vim-quickrun
let g:quickrun_config = {
  \ 'tex': {
  \  'command': 'lualatex',
  \  'exec': ['%c -synctex=1 -interaction=nonstopmode -shell-escape %s']
  \ },
  \}

" Shougo/denite.vim
augroup DeniteConf
  autocmd!
  function! s:denite_my_settings() abort
    nnoremap <silent><buffer><expr> <CR> denite#do_map('do_action')
    nnoremap <silent><buffer><expr> i denite#do_map('open_filter_buffer')
    nnoremap <silent><buffer><expr> q denite#do_map('quit')
  endfunction
  function! s:denite_filter_my_settings() abort
    imap <silent><buffer> <CR> <Plug>(denite_filter_quit)<CR>
    imap <silent><buffer> <C-q> <Plug>(denite_filter_quit) 
  endfunction
  function! s:denite_lazy_function() abort
    if executable('rg')
      call denite#custom#var('file_rec', 'command',
      \ ['rg', '--files', '--glob', '!.git'])
      call denite#custom#var('grep', 'command', ['rg'])
    endif
  endfunction
  autocmd FileType denite call s:denite_my_settings()
  autocmd FileType denite-filter call s:denite_filter_my_settings()
  autocmd VimEnter * call s:denite_lazy_function()
augroup END

noremap [Denite] <Nop>
nmap <Leader>1 [Denite]
" outline
nnoremap <silent> [Denite]o :<C-u>call ActionInMainWindow(':Denite outline')<CR>
" recently opened files <Leader>1+r
nnoremap <silent> [Denite]r :<C-u>call ActionInMainWindow(':Denite file_mru')<CR>
" switch buffer <Leader>1+b
nnoremap <silent> [Denite]b :<C-u>call ActionInMainWindow(':Denite -start-filter buffer')<CR>
" open files in current dir <Leader>1+e
nnoremap <silent> [Denite]e :<C-u>call ActionInMainWindow(':DeniteBufferDir file/rec')<CR>
" open files in root dir <Leader>+re
nnoremap <silent> [Denite]re :<C-u>call ActionInMainWindow(':Denite file/rec')<CR>
" search for text in files <Leader>1+g
nnoremap <silent> [Denite]g :<C-u>call ActionInMainWindow(':DeniteBufferDir grep')<CR>
" search for text in current file <Leader>1+f
nnoremap <silent> [Denite]f :<C-u>call ActionInMainWindow(':Denite line')<CR>
" show help <Leader>1+h
nnoremap <silent> [Denite]h :<C-u>call ActionInMainWindow(':Denite help')<CR>

" lambdalisue/gina.vim
nnoremap <Leader>gs :<C-u>Gina status<CR>
nnoremap <Leader>gc :<C-u>Gina commit<CR>
nnoremap <Leader>gl :<C-u>Gina log<CR>
nnoremap <Leader>gd :<C-u>Gina diff<CR>
nnoremap <Leader>gpm :<C-u>Gina push origin master<CR>

" kassio/neoterm
nnoremap <silent> <Leader>is :<C-u>call ActionInMainWindow(':Ttoggle')<CR>
tnoremap <silent> <ESC> <C-\><C-n>
let g:neoterm_size = "10"
let g:neoterm_fixedsize = 1
let g:neoterm_default_mod = "rightbelow"
let g:neoterm_autoscroll = 1
let g:neoterm_term_per_tab = 1

" mbbill/undotree
nnoremap <Leader>pt :UndotreeToggle<CR>

" Shougo/defx.nvim
function! RunDefx(new) abort
  let l:opts = '-split=vertical -winwidth=25 -show-ignored-files -columns=indent:mark:icons:filename -' . (a:new ? 'new' : 'toggle')
  call ActionInMainWindow(":Defx " . l:opts)
endfunction

noremap [Defx] <Nop>
nmap <Leader>2 [Defx]
" open defx
nnoremap <silent> [Defx]e :<C-u>call RunDefx(0)<CR>
" create new defx
nnoremap <silent> [Defx]en :<C-u>call RunDefx(1)<CR>

augroup DefxConf
  function! s:defx_my_settings() abort
    nnoremap <silent><buffer><expr> <CR> defx#is_directory() ? defx#do_action('open_or_close_tree') : defx#do_action('drop')
    nnoremap <silent><buffer><expr> <Space> defx#is_directory() ? defx#do_action('open_directory') : defx#do_action('drop')
    nnoremap <silent><buffer><expr> e defx#do_action('new_file')
    nnoremap <silent><buffer><expr> m defx#do_action('move')
    nnoremap <silent><buffer><expr> d defx#do_action('remove')
    nnoremap <silent><buffer><expr> r defx#do_action('rename')
    nnoremap <silent><buffer><expr> yy defx#do_action('yank_path')
    nnoremap <silent><buffer><expr> ~ defx#do_action('cd')
    nnoremap <silent><buffer><expr> .. defx#do_action('cd', ['..'])

    " rarely use action
    nnoremap <silent><buffer><expr> C defx#do_action('copy')
    nnoremap <silent><buffer><expr> P defx#do_action('paste')
    nnoremap <silent><buffer><expr> cd defx#do_action('change_vim_cwd')

    setlocal signcolumn=no
    setlocal cursorline
  endfunction
  autocmd FileType defx call s:defx_my_settings()
  autocmd BufWritePost * call defx#redraw()
  autocmd VimEnter * call execute(':normal [Defx]e')
  autocmd TabNewEntered * call execute(':normal [Defx]en')
augroup END

" kristijanhusak/defx-icons

" rakr/vim-one
colorscheme one
set background=dark

" itchyny/lightline.vim
let g:lightline = { 'colorscheme': 'one' }
let g:lightline.active = {
  \ 'left': [['mode'], ['readonly', 'currentdir', 'filename', 'modified'], ['gitbranch', 'gitstatus']],
  \ 'right': [['linter_errors', 'linter_warnings', 'linter_ok'], ['lineinfo'], ['fileformat', 'fileencoding', 'filetype']]
  \ }
let g:lightline.component = {
  \ 'gitstatus': '%{gina#component#repo#branch()!=""?Get_git_status():""}',
  \ }
let g:lightline.component_function = {
  \ 'currentdir': 'Get_current_dir',
  \ 'filename': 'Get_current_filename',
  \ 'gitbranch': 'gina#component#repo#branch',
  \ }
let g:lightline.component_expand = {
  \ 'linter_warnings': 'lightline#ale#warnings',
  \ 'linter_errors': 'lightline#ale#errors',
  \ 'linter_ok': 'lightline#ale#ok',
  \ }
let g:lightline.component_type = {
  \ 'linter_warnings': 'warning',
  \ 'linter_errors': 'error',
  \ 'linter_ok': 'left',
  \ 'readonly': 'error',
  \ }
function! Get_current_dir() abort
  let l:file_current_dir = fnamemodify(resolve(expand('%:p')), ':h')
  let l:current_working_dir = getcwd()
  return (l:file_current_dir == l:current_working_dir) ? '' : fnamemodify(l:current_working_dir, ':~')
endfunction
function! Get_current_filename() abort
  let l:current_dir = Get_current_dir()
  if l:current_dir != ''
    return pathshorten(substitute(expand('%:p'), getcwd() . "/", "", ""))
  else
    return expand('%')
  endif
endfunction
function! Get_git_status() abort
  return printf(
    \ '+%d -%d !%d',
    \ gina#component#status#staged(),
    \ gina#component#status#unstaged(),
    \ gina#component#status#conflicted(),
    \ )
endfunction
