" options.vim
syntax enable

set encoding=utf8
set fileencodings=ucs-bom,utf-8,iso-2022-jp,euc-jp,cp932
set fileformats=unix,dos,mac

set hidden
set autoindent
set smartindent
set expandtab
set tabstop=2
set shiftwidth=2
set number
set noshowmode
set modifiable
set splitbelow

set wildmode=list:full

" for lightline
set laststatus=2

if has("termguicolors")
  set termguicolors
endif

" disable conceal
let g:tex_conceal=''

" filetype
au BufNewFile,BufRead *.s,*.S set filetype=mips
