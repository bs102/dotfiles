" init.vim
if &compatible
  set nocompatible
endif

augroup MyAutoCmd
  autocmd!
augroup END

filetype off
filetype plugin indent off

if has('nvim') && !exists(':CheckHealth')
  set rtp +=expand('~/local/share/nvim/runtime')
endif

let g:base_dir = expand('~/dotfiles/nvim')

let mapleader = "\<Space>"

function! s:load_rc(rc_file_name)
  let rc_file = g:base_dir . '/' . a:rc_file_name
  if filereadable(rc_file)
   execute 'source' rc_file
  endif
endfunction

call s:load_rc('user.vim')
call s:load_rc('functions.vim')
call s:load_rc('plugins.vim')
call s:load_rc('options.vim')
call s:load_rc('keymap.vim')

filetype plugin indent on

