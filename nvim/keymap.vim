" keymap.vim
if $VIM_KEYBOARD_LAYOUT == "us"
  noremap ; :
  noremap : ;
  inoremap <C-k> <Up>
  inoremap <C-j> <Down>
  inoremap <C-h> <Left>
  inoremap <C-l> <Right>
endif

" template
function! Insert_template_content() abort
  let l:template_dir = g:base_dir . '/template'
  let l:template_file = l:template_dir . '/template.' . expand('%:e')
  if filereadable(l:template_file)
    silent exe 'r ' . l:template_file
    silent exe line(".") - 1 . ' s/^\n//'
  endif
endfunction
nnoremap <Leader>t :<C-u>call Insert_template_content()<CR>

" reload vimrc
nnoremap <Leader>vr :<C-u>so $MYVIMRC<CR>
