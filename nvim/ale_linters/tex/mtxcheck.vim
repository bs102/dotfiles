" Author: bs102 <bs102@outlook.com>
" Description: mtx-check for LaTeX(ConTeXt) files

call ale#Set('tex_mtxcheck_executable', 'mtxrun')

function! ale_linters#tex#mtxcheck#Handle(buffer, lines) abort
   let l:pattern = '^[ ]*\(\d\+\)[ ]*\(.\+\)$'
   let l:output = []

   for l:match in ale#util#GetMatches(a:lines, l:pattern)
      call add(l:output, {
      \   'lnum': l:match[1] + 0,
      \   'text': l:match[2],
      \   'type': 'W',
      \})
   endfor

   return l:output
endfunction

call ale#linter#Define('tex', {
\   'name': 'mtxcheck',
\   'executable_callback': ale#VarFunc('tex_mtxcheck_executable'),
\   'command': '%e --script check %t',
\   'callback': 'ale_linters#tex#mtxcheck#Handle',
\})
